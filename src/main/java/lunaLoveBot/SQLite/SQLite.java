package lunaLoveBot.SQLite;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import twitter4j.Status;

public class SQLite {
    private static Connection connection = null;
    //private static Statement statement = null;
    private static List<Long> userID = new ArrayList<Long>();
    private final static String DBPath = "./Database/";
    private final static String DBFile = "twitter.db";


    /**
     * データベースに接続
     */
    public void sqliteConnect(){
        boolean notFound = false;

        //ファイルがない場合は作成
        File dbFile = new File(DBPath + DBFile);
        if (!dbFile.exists()){
            File path = new File(DBPath);
            path.mkdir();
            System.out.println("mkdir: " + path.getPath());
            notFound = true;
        }


        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }

        //接続
        Statement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:" + DBPath + DBFile);
            statement = connection.createStatement();
            //30秒でタイムアウト
            statement.setQueryTimeout(30);

            //ファイルがなかった場合で、生成した時テーブルを作る
            if (notFound){
                System.out.print("Creating Database....");
                statement.executeUpdate("CREATE TABLE [tl] ([StatusID] INTEGER NOT NULL UNIQUE,[UserID] INTEGER NOT NULL,[InReplyToID] INTEGER,[InReplyToUserID] INTEGER,[Date] INTEGER,[isRetweet] BOOLEAN NOT NULL DEFAULT '0',[Source] TEXT,[Text] TEXT NOT NULL,[Deleted] BOOLEAN NOT NULL DEFAULT '0',PRIMARY KEY(StatusID));");
                statement.executeUpdate("CREATE TABLE [user] ([UserID] INTEGER NOT NULL UNIQUE,[ScreenName] TEXT NOT NULL UNIQUE,[Name] TEXT NOT NULL,[CtmName] TEXT,PRIMARY KEY(UserID));");
                System.out.println("Completed!");
            }else {
                ResultSet re = statement.executeQuery("SELECT UserID FROM user;");

                while(re.next()){
                    userID.add(re.getLong("UserID"));
                }
                re.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * SQL文を実行する
     * @param sql
     * @param objects
     * @throws SQLException
     */
    public static void write(String sql, Object... objects) throws SQLException {
        PreparedStatement preSta = connection.prepareStatement(sql);

        for (int i = 0; i < objects.length; i++) {
            preSta.setObject(i+1, objects[i]);
        }

        preSta.executeUpdate();
    }

    /**
     * SQL文を実行し結果を得る
     * @param sql
     * @param objects
     * @return ResultSet
     * @throws SQLException
     */
    public static ResultSet read(String sql, Object... objects) throws SQLException {
        PreparedStatement preSta = connection.prepareStatement(sql);

        for (int i = 0; i < objects.length; i++) {
            preSta.setObject(i+1, objects[i]);
        }

        return preSta.executeQuery();
    }

    /**
     * ツイートをDBに挿入
     * @param status
     */
    public static void insertTweet(Status status){
        long StatusID = status.getId();
        long UserID = status.getUser().getId();
        long InReplyToID = status.getInReplyToStatusId();
        long InReplyToUserID = status.getInReplyToUserId();
        Date Date = status.getCreatedAt();
        boolean isRetweet = status.isRetweet();
        int rt = 0;
        if (isRetweet){
            rt = 1;
        }

        String Source = status.getSource();
        String Text = status.getText();
        int Deleted = 0;

        try {

            if (!userID.contains(UserID)){
                //statement.executeUpdate("INSERT INTO [user]([UserID],[ScreenName],[Name],[CtmName]) VALUES('" + UserID + "','" + status.getUser().getScreenName() + "','" + status.getUser().getName() + "','" + status.getUser().getName() + "');");
                write("INSERT INTO [user]([UserID],[ScreenName],[Name],[CtmName]) VALUES(?, ?, ?, ?);", UserID, status.getUser().getScreenName(), status.getUser().getName(), status.getUser().getName());
                userID.add(UserID);
            }

            //statement.executeUpdate("INSERT INTO [tl]([StatusID],[UserID],[InReplyToID],[InReplyToUserID],[Date],[isRetweet],[Source],[Text],[Deleted]) " + 
            //        "VALUES('" + StatusID + "','" + UserID + "','" + InReplyToID + "','" + InReplyToUserID + "'," + Date.getTime() + "," + rt + ",'" + Source + "','" + Text + "'," + Deleted + ");");

            write("INSERT INTO [tl]([StatusID],[UserID],[InReplyToID],[InReplyToUserID],[Date],[isRetweet],[Source],[Text],[Deleted]) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);", StatusID, UserID, InReplyToID, InReplyToUserID, Date.getTime(), rt, Source, Text, Deleted);

        } catch (SQLException e) {
            System.out.println("DBInsertError:StatusID:" + StatusID + "(" + e.getMessage() +  ")");
        }
    }

    /**
     * SQLiteの色々閉じる
     * @throws SQLException 
     */
    public static void close() throws SQLException{
        connection.close();
    }


    /**
     * カスタム設定ネームを取得する
     * @param userID long
     * @return カスタムユーザネーム
     */
    public static String getCtmName(long userID){
        ResultSet re = null;
        String s = null;
        try {

            //re = statement.executeQuery("SELECT CtmName FROM user WHERE UserID = " + userID + ";");
            re = read("SELECT CtmName FROM user WHERE UserID = ?;", userID);
            while (re.next()) {
                s = re.getString(1);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
                try {
                    re.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }


        return s;
    }

    /**
     * DBにある名前を取得する
     * @param userID long
     * @return DBにある名前
     */
    public static String getName(long userID){
        ResultSet re = null;
        String s = null;
        try {

            //re = statement.executeQuery("SELECT Name FROM user WHERE UserID = " + userID + ";");
            re = read("SELECT Name FROM user WHERE UserID = ?;", userID);
            while (re.next()) {
                s = re.getString(1);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            try {
                re.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return s;
    }

    /**
     * カスタムネーム保存
     * @param userID long
     * @param name String
     */
    public static void setCtmName(long userID, String name){
        try {

            //statement.executeUpdate("UPDATE [user] SET [CtmName] = '" + name +"'  WHERE [UserID] = '" + userID + "';");
            write("UPDATE [user] SET [CtmName] = ? WHERE [UserID] = ?;", name, userID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 名前保存
     * @param userID long
     * @param name String
     */
    public static void setName(long userID, String name){
        //SQLインジェクション対策
        try {

            //statement.executeUpdate("UPDATE [user] SET [Name] = '" + name +"'  WHERE [UserID] = '" + userID + "';");
            write("UPDATE [user] SET [Name] = ? WHERE [UserID] = ?;", name, userID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * パラメータのstatusのリプライ先IDを得る
     * @param statusId
     * @return InReplyTostatusId
     */
    public static long getInReplyToStatusId(long statusId){
        long l = -1L;
        ResultSet re = null;
        try {

            //re = statement.executeQuery("SELECT InReplyToID FROM tl WHERE StatusID= '" + statusId + "';");
            re = read("SELECT InReplyToID FROM tl WHERE StatusID= ?;", statusId);
            while (re.next()) {
                l = re.getLong(1);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            try {
                re.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return l;
    }

    /**
     * パラメータのstatusのリプライ先UserIDを得る
     * @param statusId
     * @return InReplyToUserId
     */
    public static long getInReplyToUserId(long statusId){
        long l = -1L;
        ResultSet re = null;
        try {

            //re = statement.executeQuery("SELECT InReplyToUserID FROM tl WHERE StatusID= '" + statusId + "';");
            re = read("SELECT InReplyToUserID FROM tl WHERE StatusID= ?;", statusId);
            while (re.next()) {
                l = re.getLong(1);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            try {
                re.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return l;
    }
}












