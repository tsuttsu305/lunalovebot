package lunaLoveBot.OAuth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import lunaLoveBot.LunaLoveBot;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;



public class OAuth {

    public void oAuth() throws TwitterException, IOException{
        LunaLoveBot.twitter = TwitterFactory.getSingleton();
        LunaLoveBot.twitter.setOAuthConsumer(LunaLoveBot.getProc().getConsumerKey(), LunaLoveBot.getProc().getConsumerSecret());
        RequestToken requestToken = LunaLoveBot.twitter.getOAuthRequestToken();
        AccessToken accessToken = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (null == accessToken) {
            System.out.println("Open the following URL and grant access to your account:");
            System.out.println(requestToken.getAuthorizationURL());
            System.out.print("Enter the PIN(if aviailable) or just hit enter.[PIN]:");
            String pin = br.readLine();
            if(pin == null){
                System.out.println("Error! ");
                return;
            }
            try{
                if(pin.length() > 0){
                    accessToken = LunaLoveBot.twitter.getOAuthAccessToken(requestToken, pin);
                }else{
                    accessToken = LunaLoveBot.twitter.getOAuthAccessToken();
                }
            } catch (TwitterException te) {
                if(401 == te.getStatusCode()){
                    System.out.println("Unable to get the access token.");
                }else{
                    te.printStackTrace();
                }
            }
        }

        //トークンを保存
        storeAccessToken(LunaLoveBot.twitter.verifyCredentials().getId() , accessToken);

    }

    private void storeAccessToken(long l, AccessToken accessToken){
        LunaLoveBot.getProc().getProp().setProperty("token", accessToken.getToken()); //accessToken.getToken() を保存
        LunaLoveBot.getProc().getProp().setProperty("tokenSecret", accessToken.getTokenSecret()); //accessToken.getTokenSecret() を保存
        LunaLoveBot.getProc().getProp().setProperty("OAuth", "true");

        LunaLoveBot.getProc().saveProp();

        LunaLoveBot.getLogger().info("キーを保存しました。");
    }
}

