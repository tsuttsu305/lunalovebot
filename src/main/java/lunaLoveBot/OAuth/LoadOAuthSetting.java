package lunaLoveBot.OAuth;

import lunaLoveBot.LunaLoveBot;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class LoadOAuthSetting {

    public void loadOAuth() throws Exception{
        // このファクトリインスタンスは再利用可能でスレッドセーフです
        TwitterFactory factory = new TwitterFactory();
        AccessToken accessToken = loadAccessToken();
        LunaLoveBot.twitter = factory.getInstance();
        LunaLoveBot.twitter.setOAuthConsumer(LunaLoveBot.getProc().getConsumerKey(), LunaLoveBot.getProc().getConsumerSecret());
        LunaLoveBot.twitter.setOAuthAccessToken(accessToken);


    }

    private static AccessToken loadAccessToken(){
        String token = LunaLoveBot.getProc().getProp().getProperty("token");	// load from a persistent store
        String tokenSecret = LunaLoveBot.getProc().getProp().getProperty("tokenSecret");	// load from a persistent store
        return new AccessToken(token, tokenSecret);
    }
}
