package lunaLoveBot.util;

public class ConfigurationUtil {
    private ConfigurationUtil() {
    }

    /**
     * コメント行の判定を行う。<br/>
     * また、空白行もcommentと判定する
     * @param str 判定する文字列
     * @return true == commentである
     */
    public static boolean isComment(String str){
        if (str.isEmpty()){
            return true;
        }

        char fi = str.charAt(0);
        if (fi == '#'){
            return true;
        }
        return false;
    }
}
