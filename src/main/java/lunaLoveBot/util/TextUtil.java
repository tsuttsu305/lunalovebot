package lunaLoveBot.util;

import twitter4j.User;
import lunaLoveBot.SQLite.SQLite;

public class TextUtil {
    private TextUtil(){

    }


    /**
     * ユーザ名置き換え
     * @param user Twitter User
     * @param input 置き換える文字列
     * @return 名前置き換え後文字列
     */
    public static String replaceName(User user, String input){
        String ctmName = SQLite.getCtmName(user.getId());
        String Name = SQLite.getName(user.getId());
        
        if (ctmName.matches(".*(@|＠)[0-9a-zA-Z_]{1,15}.*")){
            ctmName.replaceAll("@", "");
            ctmName.replaceAll("＠", "");
        }
        if (Name.matches(".*(@|＠)[0-9a-zA-Z_]{1,15}.*")){
            Name.replaceAll("@", "");
            ctmName.replaceAll("＠", "");
        }
        
        //DBのNameとCtmNameが同じ場合。Ctmは未登録
        if (ctmName.equals(Name)){
            if (!user.getName().equals(Name)){
                SQLite.setCtmName(user.getId(), user.getName());
                SQLite.setName(user.getId(), user.getName());
            }
            String n = user.getName().replaceAll("@", "");
            n = n.replaceAll("＠", "");
            input = input.replaceAll("&n", n);
            input = input.replaceAll("&N", n);
        }else{
            ctmName = ctmName.replaceAll("@", "");
            ctmName = ctmName.replaceAll("＠", "");
            input = input.replaceAll("&n", ctmName);
            input = input.replaceAll("&N", ctmName);
        }
        
        return input;
    }
}
