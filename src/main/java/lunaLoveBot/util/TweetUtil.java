package lunaLoveBot.util;

import lunaLoveBot.LunaLoveBot;
import twitter4j.TwitterException;

public class TweetUtil {
    private TweetUtil() {
    }

    /**
     * 
     * ツイートします
     * <br/>String[]
     * @param arg
     * @return true == SUCCESS
     */
    public static boolean tweet(String[] arg){

        String msg = arg[0];
        if(arg.length != 1){
            for (int i = 1;i < arg.length;i++){
                msg = msg + " " + arg[i];
            }
        }

        int count = msg.length();
        if(count > 140){
            System.out.println("文字数オーバーです!(" + count + "字)");
            return false;
        }

        //Status sta = null;
        try {
            //sta = 
            LunaLoveBot.twitter.updateStatus(msg);
        } catch (TwitterException e) {
            System.out.println("TweetError: " + e.getErrorMessage());
            return false;
            //e.printStackTrace();
        }
        //System.out.println("Send: " + sta.getText());
        return true;
    }

    /**
     * 
     * ツイートします
     * <br/>String
     * @param arg
     * @return true == SUCCESS
     */
    public static boolean tweet(String arg){
        int count = arg.length();
        if(count > 140){
            System.out.println("文字数オーバーです!(" + count + "字)");
            return false;
        }

        //Status sta = null;
        try {
            //sta = 
            LunaLoveBot.twitter.updateStatus(arg);
        } catch (TwitterException e) {
            System.out.println("TweetError: " + e.getErrorMessage());
            return false;
            //e.printStackTrace();
        }
        //System.out.println("Send: " + sta.getText());
        return true;
    }
}
