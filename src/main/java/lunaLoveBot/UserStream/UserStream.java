package lunaLoveBot.UserStream;

import lunaLoveBot.LunaLoveBot;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;



public class UserStream extends Thread{

    public void run()
    {
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setOAuthConsumerKey(LunaLoveBot.getProc().getConsumerKey());
        builder.setOAuthConsumerSecret(LunaLoveBot.getProc().getConsumerSecret());
        builder.setOAuthAccessToken(LunaLoveBot.getProc().getProp().getProperty("token"));
        builder.setOAuthAccessTokenSecret(LunaLoveBot.getProc().getProp().getProperty("tokenSecret"));
        //builder.setUserStreamBaseURL( "https://userstream.twitter.com/2/" );

        Configuration conf = builder.build();

        TwitterStreamFactory twitterStreamFactory = new TwitterStreamFactory(conf);
        TwitterStream twitterStream = twitterStreamFactory.getInstance();
        twitterStream.addListener(new UserStreamAdapter());
        twitterStream.user();


    }

}


