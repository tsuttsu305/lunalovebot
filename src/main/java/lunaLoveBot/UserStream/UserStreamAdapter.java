package lunaLoveBot.UserStream;

import lunaLoveBot.LunaLoveBot;
import lunaLoveBot.SQLite.SQLite;
import lunaLoveBot.Thread.ReceiveReply;
import lunaLoveBot.Thread.TLResponse;
import twitter4j.DirectMessage;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;

public class UserStreamAdapter implements UserStreamListener {

    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {


    }

    public void onScrubGeo(long userId, long upToStatusId) {


    }

    public void onStallWarning(StallWarning warning) {


    }

    public void onStatus(Status status) {
        System.out.println("[" + status.getCreatedAt().toString() + "]" + "@" + status.getUser().getScreenName() + " - " + status.getText());

        //DBへ挿入
        SQLite.insertTweet(status);

        //自分へのReplyかどうか && リツイートされていないか
        if (status.getInReplyToUserId() == LunaLoveBot.MyUserID && status.isRetweet() == false){
            if (SQLite.getInReplyToUserId(status.getInReplyToStatusId()) != -1 || 
                    SQLite.getInReplyToStatusId(status.getInReplyToStatusId()) != -1){
                //System.out.println("canceled");
                return;
            }

            ReceiveReply orp = new ReceiveReply(status);
            orp.setName("リプライ返信用");
            orp.start();
        }else if (status.getInReplyToUserId() == -1 && 
                status.getInReplyToStatusId() == -1 && status.isRetweet() == false){ //自分へのリプライ以外でRTではない
            if (!status.getText().matches(".*@[0-9a-zA-Z_]{1,15}.*")){
                TLResponse tlr = new TLResponse(status);
                tlr.setName("TLResponseThread");
                tlr.start();
            }
        }

    }

    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {


    }

    public void onException(Exception ex) {


    }

    public void onDeletionNotice(long directMessageId, long userId) {


    }

    public void onFriendList(long[] friendIds) {


    }

    public void onFavorite(User source, User target, Status favoritedStatus) {


    }

    public void onUnfavorite(User source, User target, Status unfavoritedStatus) {


    }

    public void onFollow(User source, User followedUser) {
        System.out.println("Follow: " + source.getScreenName() + " >> " + followedUser.getScreenName());

    }

    public void onDirectMessage(DirectMessage directMessage) {
        System.out.println("[" + directMessage.getCreatedAt().toString() + "]" + "DM:" + directMessage.getSender().getScreenName() + " - " + directMessage.getText());

        //コマンド: setname 新たな名前
        //自分が受信者の場合のみ
        if (directMessage.getRecipientId() == LunaLoveBot.MyUserID){
            if(directMessage.getText().matches("^(setname|setName) .*")){
                String[] sp = directMessage.getText().split(" ");
                String Name = sp[1];
                if (sp.length > 2){
                    for (int i = 1;i < sp.length;i++){
                        Name = Name + " " +  sp[i];
                    }
                }

                if (Name.matches(".*@[0-9a-zA-Z_]{1,15}.*")){
                    try {
                        LunaLoveBot.twitter.sendDirectMessage(directMessage.getSenderId(), "使用できない文字列が含まれています。");
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                }else{

                    SQLite.setCtmName(directMessage.getSenderId(), Name);
                    SQLite.setName(directMessage.getSenderId(), directMessage.getSender().getName());

                    try {
                        LunaLoveBot.twitter.sendDirectMessage(directMessage.getSenderId(), Name + "で登録しました。");
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public void onUserListMemberAddition(User addedMember, User listOwner,
            UserList list) {


    }

    public void onUserListMemberDeletion(User deletedMember, User listOwner,
            UserList list) {


    }

    public void onUserListSubscription(User subscriber, User listOwner,
            UserList list) {


    }

    public void onUserListUnsubscription(User subscriber, User listOwner,
            UserList list) {


    }

    public void onUserListCreation(User listOwner, UserList list) {


    }

    public void onUserListUpdate(User listOwner, UserList list) {


    }

    public void onUserListDeletion(User listOwner, UserList list) {


    }

    public void onUserProfileUpdate(User updatedUser) {


    }

    public void onBlock(User source, User blockedUser) {


    }

    public void onUnblock(User source, User unblockedUser) {


    }

}
