package lunaLoveBot.Thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lunaLoveBot.LunaLoveBot;
import lunaLoveBot.util.ReplyUtil;
import lunaLoveBot.util.TextUtil;
import twitter4j.Status;

public class TLResponse extends Thread{
    private Status status = null;

    public TLResponse(Status status) {
        this.status = status;
    }

    @Override
    public void run(){
        //特定のツイートに対する返信


        //標準
        defaultReply();
    }

    private void defaultReply() {
        List<String> matchRexList = new ArrayList<String>();
        List<String> matchTLResList = new ArrayList<String>();
        boolean flag = false;

        //一致する表現をリストに追加
        for (String rex : LunaLoveBot.getTLResponseTextReader().getRexList()) {
            if (status.getText().matches(rex)){
                matchRexList.add(rex);
                flag = true;
            }
        }

        //一致がなければ終了
        if (flag == false){
            return;
        }

        //一致した表現の中にある返信文をListに格納

        for (String s : LunaLoveBot.getTLResponseTextReader().getTLTextList()) {
            for (String rex : matchRexList) {
                String[] str = s.split(":");

                if (str[0].equals(rex)){
                    String msg = str[1];
                    if(str.length >= 3){
                        for (int i = 2;i < str.length;i++){
                            msg = msg + ":" + str[i];
                        }
                    }

                    matchTLResList.add(msg);
                }

            }
        }

        //乱数生成
        int r = matchTLResList.size();
        Random rn = new Random();
        int rnd = rn.nextInt(r);
        //出力Message格納
        String output = matchTLResList.get(rnd);

        //&Nを相手の名前に
        output = TextUtil.replaceName(status.getUser(), output);

        output = "@" + status.getUser().getScreenName() + " " + output;

        ReplyUtil.delayReply(status.getId(), output, 20 *1000);
    }
}


























