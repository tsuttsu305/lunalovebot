package lunaLoveBot.Thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lunaLoveBot.LunaLoveBot;
import lunaLoveBot.util.ReplyUtil;
import lunaLoveBot.util.TextUtil;
import twitter4j.Status;

public class ReceiveReply extends Thread {
    private Status status = null;
    public ReceiveReply(Status status) {
        this.status = status;
    }

    public void run(){
        if (status.getUser().getId() == LunaLoveBot.MyUserID){
            return;
        }

        List<String> matchRexList = new ArrayList<String>();
        List<String> matchReplyList = new ArrayList<String>();
        boolean flag = false;
        //一致する表現をリストに追加
        for (String rex : LunaLoveBot.getReplyTextReader().getRexList()) {
            if (status.getText().matches(rex)){
                matchRexList.add(rex);
                flag = true;
            }
        }

        if (flag == false){
            matchRexList.add("default");
        }

        //一致した表現の中にある返信文をListに格納
        //一致したのがない場合はdefaultを使用する

        for (String s : LunaLoveBot.getReplyTextReader().getReplyTextList()) {
            for (String rex : matchRexList) {
                String[] str = s.split(":");

                if (str[0].equals(rex)){
                    String msg = str[1];
                    if(str.length >= 3){
                        for (int i = 2;i < str.length;i++){
                            msg = msg + ":" + str[i];
                        }
                    }

                    matchReplyList.add(msg);
                }

            }
        }

        //乱数生成
        int r = matchReplyList.size();
        Random rn = new Random();
        int rnd = rn.nextInt(r);
        //出力Message格納
        String output = matchReplyList.get(rnd);

        //&Nを相手の名前に
        output = TextUtil.replaceName(status.getUser(), output);

        output = "@" + status.getUser().getScreenName() + " " + output;

        ReplyUtil.delayReply(status.getId(), output, 20 *1000);
        //ReplyUtil.delayReply(status.getId(), output, 20);

        /*//テストMessage
		String test = "……君は意地悪だな";
		test = "@" + status.getUser().getScreenName() + " " + test;
		Reply rp = new Reply();
		try {
			rp.reply(status.getId(), test);
		} catch (TwitterException e) {
			e.printStackTrace();
		}*/


    }

}



















