package lunaLoveBot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Timer;
import java.util.logging.Logger;

import lunaLoveBot.Conf.FollowerListConf;
import lunaLoveBot.Conf.NGSourceConfReader;
import lunaLoveBot.Conf.PropertiesConf;
import lunaLoveBot.Conf.ReplyTextReader;
import lunaLoveBot.Conf.ScenarioTextReader;
import lunaLoveBot.Conf.TLResponseTextReader;
import lunaLoveBot.OAuth.LoadOAuthSetting;
import lunaLoveBot.OAuth.OAuth;
import lunaLoveBot.SQLite.SQLite;
import lunaLoveBot.Thread.FollowBackTimer;
import lunaLoveBot.Thread.ScenarioTimer;
import lunaLoveBot.UserStream.UserStream;
import lunaLoveBot.util.TweetUtil;
import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
/**
 * 
 * @author tsuttsu305
 *
 */
public class LunaLoveBot {

    private static Logger logger = Logger.getLogger("LunaLoveBot");
    private static PropertiesConf proc = null;

    public static Twitter twitter = TwitterFactory.getSingleton();
    public static long MyUserID;

    private static ScenarioTextReader scr = null;
    private static ReplyTextReader rtr = null;
    private static FollowerListConf flc = null;
    private static TLResponseTextReader tlr = null;
    private static NGSourceConfReader NGS = null;

    private static Timer scenarioTimer = null;
    private static UserStream us = null;


    /**
     * @param args
     */
    public static void main(String[] args) {
        SQLite sql = new SQLite();
        sql.sqliteConnect();

        //プロパティとConfigをロード
        loadSetting();

        //認証系
        regOAuth();
        try {
            MyUserID = twitter.getId();
        } catch (IllegalStateException e1) {
            e1.printStackTrace();
        } catch (TwitterException e1) {
            e1.printStackTrace();
        }

        //UserStreamStart
        us = new UserStream();
        us.start();

        //TimerStart
        scenarioTimer = new Timer("ScenarioTimer");
        long scenarioDelay = Long.valueOf(getProc().getProp().getProperty("Timer")) *1000 *60;
        scenarioTimer.schedule(new ScenarioTimer(), 5000, scenarioDelay);

        //FollowBackTimer
        Timer fbt = new Timer("FollowBackTimer");
        fbt.schedule(new FollowBackTimer(), 0, Long.valueOf(getProc().getProp().getProperty("FBTimer")) *1000 *60);



        //コマンド入力待ち
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        for(;;){

            String inputCmd = null;
            try {
                inputCmd = input.readLine();
            } catch (IOException e) {
                getLogger().warning("入力エラー");
                continue;
            }

            //コマンド部と引数部に分割
            String[] temp = inputCmd.split(" ");
            String cmd = temp[0];	//コマンド
            String[] arg = new String[temp.length-1];	//引数

            for (int i = 1;i<temp.length;i++){
                arg[i-1] = temp[i];
            }

            //コマンド判定
            commandRun(cmd, arg);
        }
    }

    /**
     * プロパティとConfigをロード
     */
    private static void loadSetting() {
        //プロパティロード
        proc = new PropertiesConf();
        proc.loadProp();

        //scenario.txtロード
        scr = new ScenarioTextReader();
        scr.load();

        //reply.txtロード
        rtr = new ReplyTextReader();
        rtr.load();

        //tl.txtロード
        tlr = new TLResponseTextReader();
        tlr.load();

        //follower.txtロード
        flc = new FollowerListConf();
        flc.load();

        //NGSource.txtロード
        NGS = new NGSourceConfReader();
        NGS.load();
    }


    private static void commandRun(String cmd, String[] arg) {
        //再認証コマンド
        if (cmd.equalsIgnoreCase("oauth")){
            twitter = null;
            OAuth oa = new OAuth();
            try {
                oa.oAuth();
            } catch (TwitterException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //手動ツイートコマンド
        }else if (cmd.equalsIgnoreCase("tweet")){
            TweetUtil.tweet(arg);

            //ランダムセリフツイート
        }else if (cmd.equalsIgnoreCase("sc")){
            TweetUtil.tweet(getScenarioTextReader().getLunaStr());

            //設定再読み込み
        }else if (cmd.equalsIgnoreCase("load")){
            if (arg[0].equalsIgnoreCase("sc")){
                getScenarioTextReader().load();

            }else if (arg[0].equalsIgnoreCase("reply")){
                getReplyTextReader().load();

            }else if (arg[0].equalsIgnoreCase("followlist")){
                flc.load();

            }else if (arg[0].equalsIgnoreCase("tl")){
                tlr.load();
            }


        }else if(cmd.equalsIgnoreCase("crFlist")){
            try {
                IDs id = twitter.getFollowersIDs(MyUserID, -1L);
                long[] ids = id.getIDs();
                System.out.println(ids.length);
                for (int i = 0;i < ids.length;i++){
                    System.out.println(ids[i]);
                    getFLConf().getFolloerIDList().add(ids[i]);
                }
                getFLConf().save();
            } catch (TwitterException e) {
                e.printStackTrace();
            }

        }else if(cmd.equalsIgnoreCase("exit")){
            System.out.println("Botを停止します...");
            try {
                SQLite.close();
            } catch (SQLException e) {
            }

            System.exit(0);
        }
    }


    private static void regOAuth() {
        //認証済みかどうかを判定
        if (proc.getProp().getProperty("OAuth").equalsIgnoreCase("true")){
            LoadOAuthSetting loa = new LoadOAuthSetting();
            try {
                loa.loadOAuth();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            OAuth oa = new OAuth();
            try {
                oa.oAuth();
            } catch (TwitterException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static Logger getLogger() {
        return logger;
    }


    public static PropertiesConf getProc() {
        return proc;
    }


    public static ScenarioTextReader getScenarioTextReader() {
        return scr;
    }


    public static ReplyTextReader getReplyTextReader() {
        return rtr;
    }


    public static FollowerListConf getFLConf() {
        return flc;
    }


    public static TLResponseTextReader getTLResponseTextReader() {
        return tlr;
    }

    public static NGSourceConfReader getNGSourceConfReader() {
        return NGS;
    }

}