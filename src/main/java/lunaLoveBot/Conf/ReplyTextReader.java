package lunaLoveBot.Conf;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lunaLoveBot.util.ConfigurationUtil;

public class ReplyTextReader {
    /**
     * 同一のものはない
     */
    private List<String> rex = new ArrayList<String>();
    private List<String> replytxt = new ArrayList<String>();


    /**
     * Reply用Txt読み込み
     */
    public void load(){
        boolean chk = true;
        rex.clear();
        replytxt.clear();


        FileReader fr = null;
        BufferedReader br = null;

        try {
            fr = new FileReader("reply.txt");
            br = new BufferedReader(fr);

            String rstr = "";
            while((rstr = br.readLine()) != null){
                if (ConfigurationUtil.isComment(rstr) == false){

                    String[] tmp = rstr.split(":");
                    if (!(tmp.length >= 2)){
                        continue;
                    }
                    rexAdd(tmp[0]);
                    replytxt.add(rstr);
                }
            }


        } catch (FileNotFoundException e) {
            System.out.println("reply.txtが見つかりません。");
            chk = false;
        } catch (IOException e) {
            System.out.println("reply.txt読み込みエラー");
            chk = false;
            e.printStackTrace();
        } finally{
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (chk){
            System.out.println("reply.txtを読み込みました。 項目数: " + replytxt.size());
        }

        //デバッグ用処理
        /*
		System.out.println("ReplyList");
		for(String s:replytxt){
			System.out.println(s);
		}
		System.out.println("rexList");
		for(String s:rex){
			System.out.println(s);
		}
         */
    }

    private void rexAdd(String rex){
        if (this.rex.contains(rex) == false){
            this.rex.add(rex);
        }
    }

    public List<String> getRexList() {
        return rex;
    }

    public List<String> getReplyTextList() {
        return replytxt;
    }
}
