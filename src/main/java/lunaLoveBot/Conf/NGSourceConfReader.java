package lunaLoveBot.Conf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lunaLoveBot.util.ConfigurationUtil;

public class NGSourceConfReader {
    private List<String> slist = new ArrayList<String>();


    private File fi = new File("NGSource.txt");

    /**
     * tl.txt読み込み
     */
    public void load(){
        boolean chk = true;
        slist.clear();

        if (!fi.exists()){
            FileWriter fw = null;
            BufferedWriter bw = null;

            try {
                fw = new FileWriter(fi);
                bw = new BufferedWriter(fw);

                //バッファに出力
                bw.write("");

                //書き込み
                bw.flush();
            } catch (IOException e) {
                System.out.println(fi.getName() + "書き込みエラー");
            }finally{
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        FileReader fr = null;
        BufferedReader br = null;

        try {
            fr = new FileReader(fi);
            br = new BufferedReader(fr);

            String rstr = "";
            while((rstr = br.readLine()) != null){
                if (ConfigurationUtil.isComment(rstr) == false){
                    slist.add(rstr);
                }
            }


        } catch (FileNotFoundException e) {
            System.out.println("NGSource.txtが見つかりません。");
            chk = false;
        } catch (IOException e) {
            System.out.println("NGSource.txt読み込みエラー");
            chk = false;
            e.printStackTrace();
        } finally{
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (chk){
            System.out.println("NGSource.txtを読み込みました。 項目数: " + slist.size());
        }

        /*for (String s : slist) {
			System.out.println(s);
		}*/
    }

    public List<String> getNGSList() {
        return slist;
    }
}
