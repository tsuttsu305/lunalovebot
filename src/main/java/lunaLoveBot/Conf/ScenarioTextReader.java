package lunaLoveBot.Conf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lunaLoveBot.util.ConfigurationUtil;

public class ScenarioTextReader {
    //自動発言用セリフ格納List
    private List<String> scenarioList = new ArrayList<String>();

    private File fi = new File("scenario.txt");


    public void load(){

        if (!fi.exists()){
            FileWriter fw = null;
            BufferedWriter bw = null;

            try {
                fw = new FileWriter(fi);
                bw = new BufferedWriter(fw);

                //バッファに出力
                bw.write("");

                //書き込み
                bw.flush();
            } catch (IOException e) {
                System.out.println(fi.getName() + "書き込みエラー");
            }finally{
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        boolean chk = true;

        scenarioList.clear();

        FileReader fr = null;
        BufferedReader br = null;

        try {
            fr = new FileReader("scenario.txt");
            br = new BufferedReader(fr);

            String rstr = "";
            while((rstr = br.readLine()) != null){
                if (ConfigurationUtil.isComment(rstr) == false){
                    scenarioList.add(rstr);
                }
            }


        } catch (FileNotFoundException e) {
            System.out.println("scenario.txtが見つかりません。");
            chk = false;
        } catch (IOException e) {
            System.out.println("scenario.txt読み込みエラー");
            chk = false;
            e.printStackTrace();
        } finally{
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (chk){
            System.out.println("scenario.txtを読み込みました。 項目数: " + scenarioList.size());
        }

        //デバッグ用処理
        /*
		for(String s:scenarioList){
			System.out.println(s);
		}
         */
    }

    /**
     * リストからランダムで1つ文を返す
     * @return ルナ様のセリフ
     */
    public String getLunaStr(){
        int c = scenarioList.size();
        Random rn = new Random();
        int ran = rn.nextInt(c);
        return scenarioList.get(ran);
    }

}
