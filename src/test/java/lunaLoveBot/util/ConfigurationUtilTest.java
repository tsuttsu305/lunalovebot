package lunaLoveBot.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConfigurationUtilTest {
	
	@Test
	public void test() {
		assertEquals(ConfigurationUtil.isComment("#aaaa"), true);
		assertEquals(ConfigurationUtil.isComment("aaaa"), false);
	}
	
}
